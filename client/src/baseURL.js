const baseURL = window.location.hostname === `localhost`
    ? `http://localhost:4000`
    : `http://167.71.11.209/server`;

export default baseURL;
